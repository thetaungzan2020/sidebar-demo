package com.example.userdetailssecuritydemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserdetailsSecurityDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserdetailsSecurityDemoApplication.class, args);
	}

}
