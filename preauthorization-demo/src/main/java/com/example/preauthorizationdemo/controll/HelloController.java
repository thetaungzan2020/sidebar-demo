package com.example.preauthorizationdemo.controll;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.preauthorizationdemo.service.NameService;

@RestController
public class HelloController {

	@Autowired
	private NameService nameService;
	
	@GetMapping("/hello")
	public String hello() {
		return "Hello ".concat(nameService.getName());
	}
	
	@GetMapping("/secret/names/{name}")
	public List<String> nickName(@PathVariable String name) {
		return nameService.getSecretName(name);
		
	}
}
