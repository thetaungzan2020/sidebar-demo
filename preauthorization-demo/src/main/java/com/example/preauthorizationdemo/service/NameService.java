package com.example.preauthorizationdemo.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
public class NameService {
	
	private Map<String, List<String>> secretName() {
		Map<String, List<String>> map = new HashMap<String,List<String>>();
		
		map.put("john",Arrays.asList("Wild World","Moon Pig"));
		map.put("emma",Arrays.asList("Blue mountain"));
		return map;
	}
	
	@PreAuthorize("hasAuthority('ROLE_WRITE')")
	public String getName() {
		return "John William";
	}
	
	@PreAuthorize("#name == authentication.principal.username")
	public List<String> getSecretName(String name) {
		return secretName().get(name);
	}
}
