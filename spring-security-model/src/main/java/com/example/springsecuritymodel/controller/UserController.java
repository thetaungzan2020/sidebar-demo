package com.example.springsecuritymodel.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.example.springsecuritymodel.model.User;
import com.example.springsecuritymodel.repository.UserRepository;

@Controller
public class UserController {

	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private PasswordEncoder encoder;

	@GetMapping("/users")
	@ResponseBody
	public Iterable<User> view() {
		return userRepo.findAll();
	}

	@GetMapping("/users/create")
	public ModelAndView create() {
		return new ModelAndView("user-create", "user", new User());
	}

	@PostMapping("/users/create")
	public String create(@ModelAttribute @Valid User user, BindingResult bs) {

		if (bs.hasErrors()) {
			return "user-create";
		} else {
			user.setPassword(encoder.encode(user.getPassword()));
			userRepo.save(user);
		}

		return "redirect:/users/create";
	}
	
	@GetMapping("/users/delete/{id}")
	public String delete(@PathVariable Integer id) {
		userRepo.deleteById(id);
		
		return "redirect:/users";
	}
}
