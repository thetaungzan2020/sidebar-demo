package com.example.springsecuritymodel.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {
	
	@GetMapping({"/","/home","/index"})
	public String home() {
		return "Welcome Spring Secuirty Sample!!!";
	}
}
