package com.example.springsecuritymodel.security.utils;

import java.util.Arrays;
import java.util.List;

public class SecurityRoles {

	public static final String ROLE_PREFIX = "ROLE_";

	public static final String SUPER_ADMIN = "SUPER_ADMIN";

	public static final String CUSTOMER_ADMIN = "CUSTOMER_ADMIN";
	public static final String CUSTOMER_CREATE = "CUSTOMER_CREATE";
	public static final String CUSTOMER_READ = "CUSTOMER_READ";
	public static final String CUSTOMER_DELETE = "CUSTOMER_DELETE";
	public static final String CUSTOMER_PAG_VIEW = "CUSTOMER_PAG_VIEW";

	public static final String EMPLOYEE_ADMIN = "EMPLOYEE_ADMIN";
	public static final String EMPLOYEE_CREATE = "EMPLOYEE_CREATE";
	public static final String EMPLOYEE_READ = "EMPLOYEE_READ";
	public static final String EMPLOYEE_DELETE = "EMPLOYEE_DELETE";
	public static final String EMPLOYEE_PAG_VIEW = "EMPLOYEE_PAG_VIEW";

	public static final String DEPARTMENT_ADMIN = "DEPARTMENT_ADMIN";
	public static final String DEPARTMENT_CREATE = "DEPARTMENT_CREATE";
	public static final String DEPARTMENT_READ = "DEPARTMENT_READ";
	public static final String DEPARTMENT_DELETE = "DEPARTMENT_DELETE";
	public static final String DEPARTMENT_PAG_VIEW = "DEPARTMENT_PAG_VIEW";
	
	
	public static List<String> getRoles() {
		List<String> roles = Arrays.asList(SUPER_ADMIN,CUSTOMER_ADMIN,CUSTOMER_CREATE,CUSTOMER_DELETE,CUSTOMER_PAG_VIEW,CUSTOMER_READ);
		return roles;
	}
}
