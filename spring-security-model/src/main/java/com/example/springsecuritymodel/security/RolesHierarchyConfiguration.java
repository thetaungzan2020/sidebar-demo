package com.example.springsecuritymodel.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;

import com.example.springsecuritymodel.security.utils.RolesHierarchyBuilder;

import static com.example.springsecuritymodel.security.utils.SecurityRoles.SUPER_ADMIN;
import static com.example.springsecuritymodel.security.utils.SecurityRoles.CUSTOMER_ADMIN;
import static com.example.springsecuritymodel.security.utils.SecurityRoles.CUSTOMER_CREATE;
import static com.example.springsecuritymodel.security.utils.SecurityRoles.CUSTOMER_READ;
import static com.example.springsecuritymodel.security.utils.SecurityRoles.CUSTOMER_DELETE;
import static com.example.springsecuritymodel.security.utils.SecurityRoles.CUSTOMER_PAG_VIEW;
import static com.example.springsecuritymodel.security.utils.SecurityRoles.DEPARTMENT_ADMIN;
import static com.example.springsecuritymodel.security.utils.SecurityRoles.DEPARTMENT_CREATE;

import static com.example.springsecuritymodel.security.utils.SecurityRoles.DEPARTMENT_READ;
import static com.example.springsecuritymodel.security.utils.SecurityRoles.DEPARTMENT_DELETE;
import static com.example.springsecuritymodel.security.utils.SecurityRoles.DEPARTMENT_PAG_VIEW;
import static com.example.springsecuritymodel.security.utils.SecurityRoles.EMPLOYEE_ADMIN;
import static com.example.springsecuritymodel.security.utils.SecurityRoles.EMPLOYEE_CREATE;
import static com.example.springsecuritymodel.security.utils.SecurityRoles.EMPLOYEE_READ;
import static com.example.springsecuritymodel.security.utils.SecurityRoles.EMPLOYEE_DELETE;
import static com.example.springsecuritymodel.security.utils.SecurityRoles.EMPLOYEE_PAG_VIEW;

@Configuration
public class RolesHierarchyConfiguration {

	@Bean
	public RoleHierarchy roleHierarchy() {

		RoleHierarchyImpl roleHierarchy = new RoleHierarchyImpl();

		roleHierarchy.setHierarchy(new RolesHierarchyBuilder().append(SUPER_ADMIN, CUSTOMER_ADMIN)
				.append(CUSTOMER_ADMIN, CUSTOMER_CREATE).append(CUSTOMER_ADMIN, CUSTOMER_READ)
				.append(CUSTOMER_ADMIN, CUSTOMER_DELETE).append(CUSTOMER_ADMIN, CUSTOMER_PAG_VIEW)

				.append(SUPER_ADMIN, EMPLOYEE_ADMIN).append(EMPLOYEE_ADMIN, EMPLOYEE_CREATE)
				.append(EMPLOYEE_ADMIN, EMPLOYEE_READ).append(EMPLOYEE_ADMIN, EMPLOYEE_DELETE)
				.append(EMPLOYEE_ADMIN, EMPLOYEE_PAG_VIEW)

				.append(SUPER_ADMIN, DEPARTMENT_ADMIN).append(DEPARTMENT_ADMIN, DEPARTMENT_CREATE)
				.append(DEPARTMENT_ADMIN, DEPARTMENT_READ).append(DEPARTMENT_ADMIN, DEPARTMENT_DELETE)
				.append(DEPARTMENT_ADMIN, DEPARTMENT_PAG_VIEW)

				.build());
		
		return roleHierarchy;
	}
}
