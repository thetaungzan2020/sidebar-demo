package com.example.springsecuritymodel.security;

import static com.example.springsecuritymodel.security.utils.SecurityRoles.EMPLOYEE_PAG_VIEW;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;

import com.example.springsecuritymodel.security.service.CustomUserDetailsService;;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {

	@Autowired
	private RoleHierarchy roleHierarchy;
	
	@Autowired
	private CustomUserDetailsService userdetailsService;
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
			
	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
		
		http.formLogin();
		http.csrf().disable();
		
		http.authorizeRequests(auth-> auth
				.expressionHandler(this.expressionHandler())
				.mvcMatchers("/","/home").permitAll()
				.mvcMatchers("/users").hasAuthority(EMPLOYEE_PAG_VIEW)
				.anyRequest().authenticated());
		
		return http.build();
	}
	
	@Bean
	@Autowired
	public AuthenticationManager authenticationManager(HttpSecurity http, DataSource dataSource) throws Exception {

		AuthenticationManagerBuilder authenticationManagerBuilder = http
				.getSharedObject(AuthenticationManagerBuilder.class);

		authenticationManagerBuilder.userDetailsService(userdetailsService).passwordEncoder(this.passwordEncoder());
		
		return authenticationManagerBuilder.build();
	}

//	@Bean
//	public AccessDecisionManager accessDecisionManager() {
//		List<AccessDecisionVoter<? extends Object>> decisionVoters=new ArrayList<>();
//		decisionVoters.add(new RoleHierarchyVoter(roleHierarchy));
//		return new AffirmativeBased(decisionVoters);
//	}
	
	@Bean
	public DefaultWebSecurityExpressionHandler expressionHandler() {
		DefaultWebSecurityExpressionHandler expressionHandler = new DefaultWebSecurityExpressionHandler();

		expressionHandler.setRoleHierarchy(roleHierarchy);

		return expressionHandler;
	}
}
