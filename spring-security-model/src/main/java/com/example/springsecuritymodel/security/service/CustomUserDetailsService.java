package com.example.springsecuritymodel.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.springsecuritymodel.repository.UserRepository;

@Service("custom_userdetails_service")
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepo;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		System.out.println("loadUserByUsername : " + username);

		System.out.println("User : " + userRepo.findUserByLoginId(username));

		UserDetails user = userRepo.findUserByLoginId(username)
								   .map(u -> User.withUsername(u.getLoginId())
										         .password(u.getPassword())
										   		 .authorities(AuthorityUtils.createAuthorityList(u.getRole()))
										   		 .build())
				                   .orElseThrow(() -> new UsernameNotFoundException("Not Register User!!"));

		return user;

	}

}
