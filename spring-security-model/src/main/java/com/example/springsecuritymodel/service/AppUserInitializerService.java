package com.example.springsecuritymodel.service;

import static com.example.springsecuritymodel.security.utils.SecurityRoles.EMPLOYEE_PAG_VIEW;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.example.springsecuritymodel.model.User;
import com.example.springsecuritymodel.repository.UserRepository;

@Component
public class AppUserInitializerService {

	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private UserRepository repo;

	@Transactional
	public void initializeUser() {

		if(repo.count() == 0) {
			User user = new User();

			user.setLoginId("DO1830");
			user.setUsername("doe");
			user.setPassword(passwordEncoder.encode("doe9918"));
			user.setEmail("doe@crop-cus.com");
			user.setAddress("KIH");
			user.setRole(EMPLOYEE_PAG_VIEW);

			repo.save(user);
		}
	}
}
