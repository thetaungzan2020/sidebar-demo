package com.example.springsecuritymodel.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "login_id")
	@NotBlank(message = "loginId can't be empty!")
	@Pattern(regexp = "[A-Za-z0-9]*", message = "login id contain illage characters.")
	private String loginId;

	@Column(name = "username")
	@NotBlank(message = "username can't be empty!")
	@Pattern(regexp = "[A-Za-z]*", message = "username contain illage characters.")
	private String username;

	@Column(name = "password")
	@NotBlank(message = "password can't be empty!")
//	@Pattern(regexp = "[^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,}$]*", message = "password contain illage characters.")
	private String password;

	@Column(name = "role")
	@NotBlank(message = "role can't be empty!")
	private String role;

	@Column(name = "email")
	@NotBlank(message = "email can't be empty!")
	@Pattern(regexp = "[^@ \\t\\r\\n]+@[^@ \\t\\r\\n]+\\.[^@ \\t\\r\\n]+", message = "email contain illage characters.")
	private String email;

	@Column(name = "address")
	@NotBlank(message = "address can't be empty!")
	@Pattern(regexp = "[\\w .\\-/,]*", message = "address contain illage characters.")
	private String address;
	
}
