package com.example.springsecuritymodel.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Data
@AllArgsConstructor
public class Department {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@NotBlank(message = "code can't be empty!")
	@Size(min = 2 , max = 5 , message = "code must have length 2 - 5 characters.")
	@Pattern(regexp = "[A-Za-z]*", message = "code contain illage characters.")
	private String code;
	
	@NotBlank(message = "name can't be empty!")
	@Pattern(regexp = "[A-Za-z]*", message = "name contain illage characters.")
	private String name;
	
	@NotBlank(message = "country can't be empty!")
	@Pattern(regexp = "[A-Za-z]*", message = "country contain illage characters.")
	private String country;
	
	
	public Department() {
		
	}
}
