package com.example.csrfdemo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	@GetMapping("/a")
	public String getEndPointA() {
		return "Work! getEndPointA";
	}
	
	@GetMapping("/a/b")
	public String getEndPointB() {
		return "Work! getEndPointB";
	}
	
	@GetMapping("/a/b/c")
	public String getEndPointC() {
		return "Work! getEndPointC";
	}
	
	@PostMapping("/a")
	public String postEndPointA() {
		return "Work! postEndPointA";
	}
}
