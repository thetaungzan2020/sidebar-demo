package com.example.csrfdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CsrfDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CsrfDemoApplication.class, args);
		/*
		 * try { SecureRandom random= SecureRandom.getInstanceStrong();
		 * 
		 * String code = String.valueOf(random.nextInt(9000)+1000);
		 * System.out.println("Random code : "+ code); } catch (NoSuchAlgorithmException
		 * e) { e.printStackTrace(); }
		 */
	}

}
