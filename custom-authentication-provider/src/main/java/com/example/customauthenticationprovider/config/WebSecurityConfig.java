package com.example.customauthenticationprovider.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;

import com.example.customauthenticationprovider.security.CustomAuthenticationProvider;

@Configuration
public class WebSecurityConfig {

    @Autowired
    private CustomAuthenticationProvider authenticationProvider;
    
    @Bean
    AuthenticationManager authenticationManager(HttpSecurity http) throws Exception {
        AuthenticationManagerBuilder authenticationManagerBuilder = http.getSharedObject(AuthenticationManagerBuilder.class);
        authenticationManagerBuilder.authenticationProvider(authenticationProvider);
        
//        authenticationManagerBuilder.jdbcAuthentication().dataSource(datasource);
        
        return authenticationManagerBuilder.build();
    }

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.httpBasic(c -> c.realmName("Custom Authentication Provider"));

        http.authorizeHttpRequests().anyRequest().authenticated();
        return http.build();
    }
}
