CREATE TABLE users(
    id INT NOT NULL AUTO_INCREAMENT,
    username VARCHAR(45) NOT NULL,
    password VARCHAR(45) NOT NULL,
    enabled INT NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE authorities(
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(45) NULL,
    authority VARCHAR(45) NULL,
    PRIMARY KEY(id)
);