package com.example.authorizationdemo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@SuppressWarnings("deprecation")
@Configuration
public class SecurityConfig {

	@Bean
	public UserDetailsService userDetailsService() {
		InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();

		UserDetails user1 = User.withUsername("thomas").password("12345").authorities("ROLE_ADMIN").build();

		UserDetails user2 = User.withUsername("jane").password("12345").authorities("ROLE_MANAGER").build();

		manager.createUser(user1);
		manager.createUser(user2);

		return manager;
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}

	@Bean
	protected SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {

		http.httpBasic(c -> c.realmName("Authorization Demo"));

//		http.authorizeHttpRequests().anyRequest().hasAuthority("WRITE");
//		http.authorizeHttpRequests().anyRequest().hasRole("ADMIN");
		http.authorizeHttpRequests()
			.mvcMatchers("/home").hasAnyRole("MANAGER")
			.mvcMatchers("/greet").hasRole("ADMIN")
			.anyRequest().authenticated();
//			.anyRequest().permitAll();
		return http.build();
	}
}
