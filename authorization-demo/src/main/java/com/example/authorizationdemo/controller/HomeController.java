package com.example.authorizationdemo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @GetMapping("/home")
    public String home() {
        return "Welcome Spring Authorization!!";
    }
    
    @GetMapping("/greet")
    public String greet() {
        return "Good Morining,Spring Authorization!!";
    }
    
    @GetMapping("/hello")
    public String hello() {
        return "Hello,Spring Authorization!!";
    }
}
